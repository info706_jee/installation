# Installations logiciels pour JEE

## Installation JDK java

Vous aurez besoin d'au moins une version de la JDK java. 
Les serveurs d'applications que nous utiliserons sont sensés fonctionner correctement 
avec les 3 dernières versions *lts* actuelles, *jdk-11*, *jdk-17*, *jdk-21*.  
Il est donc fortement conseillé d'installer une ou plusieurs de ces versions.

Remarque : il est tout à fait possible d'installer plusieurs versions de la JDK 
sur une même machine.

###  Installation manuelle

Télécharger les versions de la JDK Java (version opendjdk) qui vous intéressent, par exemple sur le site d'Adoptium : https://adoptium.net/marketplace/

Suivant votre OS des versions installables ou sous forme d'archives à extraire (zip/tar.gz) sont disponibles.

### Installation avec *winget* (windows)

1. La commande `winget search jdk` devrait vous permettre de lister les différentes variantes de la jdk disponibles : 

```
PS C:\Users\Stephane> winget search jdk
Nom                                        ID                             Version     Correspondance Source
-----------------------------------------------------------------------------------------------------------
Java SE Development Kit 18                 Oracle.JDK.18                  18.0.2.1    Tag: jdk       winget
Java SE Development Kit 17                 Oracle.JDK.17                  17.0.4.1    Tag: jdk       winget
Eclipse Temurin JDK with Hotspot 8         EclipseAdoptium.Temurin.8.JDK  8.0.345.1   Tag: jdk       winget
Eclipse Temurin JDK with Hotspot 19 (Beta) EclipseAdoptium.Temurin.19.JDK 19.0.0.36   Tag: jdk       winget
Eclipse Temurin JDK with Hotspot 18        EclipseAdoptium.Temurin.18.JDK 18.0.2.101  Tag: jdk       winget
Eclipse Temurin JDK with Hotspot 17        EclipseAdoptium.Temurin.17.JDK 17.0.4.101  Tag: jdk       winget
Eclipse Temurin JDK with Hotspot 16        EclipseAdoptium.Temurin.16.JDK 16.0.2.7    Tag: jdk       winget
Eclipse Temurin JDK with Hotspot 11        EclipseAdoptium.Temurin.11.JDK 11.0.16.8   Tag: jdk       winget
Azul ZuluFX JDK 18                         Azul.ZuluFX.18.JDK             18.32.11    Tag: jdk       winget
Azul ZuluFX JDK 17                         Azul.ZuluFX.17.JDK             17.36.13    Tag: jdk       winget
Azul Zulu JDK 9                            Azul.Zulu.9.JDK                9.0.7.1     Tag: jdk       winget
Azul Zulu JDK 8                            Azul.Zulu.8.JDK                8.64.0.19   Tag: jdk       winget
Azul Zulu JDK 7                            Azul.Zulu.7.JDK                7.56.0.11   Tag: jdk       winget
Azul Zulu JDK 6                            Azul.Zulu.6.JDK                6.22.0.3    Tag: jdk       winget
Azul Zulu JDK 18                           Azul.Zulu.18.JDK               18.32.11    Tag: jdk       winget
Azul Zulu JDK 17                           Azul.Zulu.17.JDK               17.36.15    Tag: jdk       winget
Azul Zulu JDK 16                           Azul.Zulu.16.JDK               16.32.15    Tag: jdk       winget
Azul Zulu JDK 15                           Azul.Zulu.15.JDK               15.42.15    Tag: jdk       winget
Azul Zulu JDK 13                           Azul.Zulu.13.JDK               13.50.15    Tag: jdk       winget
Azul Zulu JDK 11                           Azul.Zulu.11.JDK               11.58.17    Tag: jdk       winget
Amazon Corretto 8                          Amazon.Corretto.8              1.8.0.342   Tag: jdk       winget
Amazon Corretto 18                         Amazon.Corretto.18             18.0.2.9    Tag: jdk       winget
Amazon Corretto 17                         Amazon.Corretto.17             17.0.4.8    Tag: jdk       winget
Amazon Corretto 11                         Amazon.Corretto.11             11.0.16.8   Tag: jdk       winget
AdoptOpenJDK JDK with Hotspot 8            AdoptOpenJDK.OpenJDK.8         8.0.292.10  Tag: jdk       winget
AdoptOpenJDK JDK with Hotspot 16           AdoptOpenJDK.OpenJDK.16        16.0.1.9    Tag: jdk       winget
AdoptOpenJDK JDK with Hotspot 15           AdoptOpenJDK.OpenJDK.15        15.0.2.7    Tag: jdk       winget
AdoptOpenJDK JDK with Hotspot 14           AdoptOpenJDK.OpenJDK.14        14.0.2.12   Tag: jdk       winget
AdoptOpenJDK JDK with Hotspot 11           AdoptOpenJDK.OpenJDK.11        11.0.11.9   Tag: jdk       winget
Eclipse Temurin JDK with Hotspot 20 (Beta) EclipseAdoptium.Temurin.20.JDK 20.0.0.13   Tag: jdk       winget
ojdkbuild OpenJDK JRE 17                   ojdkbuild.openjdk.17.jre       17.0030.6.1                winget
ojdkbuild OpenJDK 17                       ojdkbuild.openjdk.17.jdk       17.0030.6.1                winget
OpenJDK 14                                 ojdkbuild.openjdk.14.jdk       14.0.1.1                   winget
OpenJDK 13                                 ojdkbuild.openjdk.13.jdk       13.0.3.1                   winget
OpenJDK JRE 11                             ojdkbuild.openjdk.11.jre       11.0.15.1                  winget
OpenJDK 11                                 ojdkbuild.openjdk.11.jdk       11.0.15.1                  winget
ojdkbuild OpenJDK JRE 8                    ojdkbuild.ojdkbuild            1.8.3221.6                 winget
Microsoft Build of OpenJDK with Hotspot 17 Microsoft.OpenJDK.17           17.0.4.101                 winget
Microsoft Build of OpenJDK with Hotspot 16 Microsoft.OpenJDK.16           16.0.2.7                   winget
Microsoft Build of OpenJDK with Hotspot 11 Microsoft.OpenJDK.11           11.0.16.101                winget
Liberica JDK 8 Full                        BellSoft.LibericaJDK.8.Full    8.0.345.1                  winget
Liberica JDK 8                             BellSoft.LibericaJDK.8         8.0.345.1                  winget
Liberica JDK 18 Full                       BellSoft.LibericaJDK.18.Full   18.0.2.101                 winget
Liberica JDK 18                            BellSoft.LibericaJDK.18        18.0.2.101                 winget
Liberica JDK 17 Full                       BellSoft.LibericaJDK.17.Full   17.0.4.101                 winget
Liberica JDK 17                            BellSoft.LibericaJDK.17        17.0.4.101                 winget
Liberica JDK 16 Full                       BellSoft.LibericaJDK.16.Full   16.0.2.7                   winget
Liberica JDK 16                            BellSoft.LibericaJDK.16        16.0.2.7                   winget
Liberica JDK 15 Full                       BellSoft.LibericaJDK.15.Full   15.0.2.10                  winget
Liberica JDK 15                            BellSoft.LibericaJDK.15        15.0.2.10                  winget
Liberica JDK 14 Full                       BellSoft.LibericaJDK.14.Full   14.0.2.13                  winget
Liberica JDK 14                            BellSoft.LibericaJDK.14        14.0.2.13                  winget
Liberica JDK 11 Full                       BellSoft.LibericaJDK.11.Full   11.0.16.101                winget
Liberica JDK 11                            BellSoft.LibericaJDK.11        11.0.16.101                winget
AdoptOpenJDK JDK with Eclipse OpenJ9 17    AdoptOpenJDK.OpenJDK.17        17.0.0.18                  winget
Update Watcher for AdoptOpenJDK            tushev.org.AJUpdateWatcher     2.0.4.0                    winget
Eclipse Temurin JRE with Hotspot 8         EclipseAdoptium.Temurin.8.JRE  8.0.345.1   Tag: openjdk   winget
Eclipse Temurin JRE with Hotspot 18        EclipseAdoptium.Temurin.18.JRE 18.0.2.101  Tag: openjdk   winget
Eclipse Temurin JRE with Hotspot 17        EclipseAdoptium.Temurin.17.JRE 17.0.4.101  Tag: openjdk   winget
Eclipse Temurin JRE with Hotspot 11        EclipseAdoptium.Temurin.11.JRE 11.0.16.8   Tag: openjdk   winget
```

2. Installez les versions qui vous intéressent (par exemples : `winget install -i Microsoft.OpenJDK.17` ou `winget install -i EclipseAdoptium.Temurin.17.JDK`)
3. Ouvrez une nouvelle fenêtre de commande et taper `java --version`. 
Cela devrait vous indiquer la version courante de java.

### Installation avec *apt* (linux Debian ou Ubuntu) : 

1. Lister les versions disponibles avec `apt search openjdk`.  
2. Installer les versions qui vous intéressent : par exemple 
`sudo apt install openjdk-17-jdk` ou 
`sudo apt install openjdk-17-jdk-headless`, 
si vous ne voulez pas installer la partie interface graphique.

## Installation Serveurs d'application Jakarta EE

La plupart des serveurs d'applications Jakarta EE sont livrés sous forme d'archives prêtes à l'emploi. 
En particulier, pour éviter d'avoir à installer une base de données SQL 
avant de pouvoir utiliser le serveur, 
ils sont souvent fournis avec une base de données java embarquée 
(*H2* pour *WildFly* et *Payara*, *Derby* pour *GlassFish*, ...).

### Installation *WildFly* (version community du serveur JEE de JBOSS)

1. Télécharger la dernière version stable de Wildfly : https://www.wildfly.org/downloads/  
   Prendre la dernière version stable compatible avec *Jakarta EE 10 Full* (a priori **WildFly 33.0.1.Final**).
2. Extraire l'archive.  
3. Dans le dossier `wildfly-33.0.1.Final` ouvrir un terminal et démarrer le serveur avec la commande : 
   `bin/standalone.sh --server-config=standalone-full.xml` 
   (ou `.\bin\standalone.bat --server-config=standalone-full.xml` avec windows)
4. Vérifier sur l'URL [http://localhost:8080](http://localhost:8080) 
   que le serveur a bien démarré.
5. Arrêter le serveur avec la commande : `./bin/jboss-cli.sh --connect command=:shutdown` 
   ou `.\bin\jboss-cli.bat --connect command=:shutdown`

### Installation de *Payara* (fork de *GlassFish*)

1. Télécharger la dernière version stable de *Payara* (version community) : 
   https://www.payara.fish/downloads/payara-platform-community-edition/ . 
   Prendre la dernière version stable compatible avec *Jakarta EE 10 Full* 
   (a priori **Payara Server 6.2024.8 (Full)**).
2. Extraire l'archive. 
3. Dans le dossier `payara6` ouvrir un terminal et démarrer le serveur avec la commande : 
   `./bin/asadmin start-domain` (ou ` .\bin\asadmin start-domain` avec windows)
4. Vérifier sur l'URL [http://localhost:8080](http://localhost:8080) 
   que le serveur a bien démarré.
5. Arrêter le serveur avec la commande : `./bin/asadmin stop-domain` ou 
   ` .\bin\asadmin stop-domain`


## Installation environnement de développement (*Eclipse IDE*)

Deux possibilité pour installer Eclipse : 

- Utiliser l'*installer* (c'est ce que recommande la fondation Eclipse).  
  L'installateur vous permet d'installer les versions de l'IDE Eclipse 
  que vous voulez (`Eclipse IDE for Enterprise Java and Web Developers`),
  tout en partageant les jars entres les différentes installations.
- Télécharger une archive correspondant à la version de l'*IDE Eclipse* qui vous intéresse
  (`Eclipse IDE for Enterprise Java and Web Developers`).  
  L'archive intègre l'ensemble de l'IDE et tous les jars correspondant au *bundle* choisi.

### Installation avec l'*installer Eclipse*

1. Télécharger l'installateur correspondant à votre système : https://www.eclipse.org/downloads/  
   L'installateur en lui-même est assez petit (une centaine de Mo).
2. Lancer l'installateur (on peut l'utiliser tel quel ou l'installer de façon plus permanente).
3. Choisir la version de l'IDE à installer.  
   Prendre le *bundle*  `Eclipse IDE for Enterprise Java and Web Developers`.  
   L'installateur va télécharger l'IDE et tous ses jars.  
   Remarques : 
   - le mode avancé permet aussi de choisir le millésime de d'IDE et de configurer plus de choses...
   - pour installer la version *2024-06* vous aurez besoin d'une machine virtuelle  *Java-21*.
4. Configurer les machines virtuelles Java installées.  
   Dans `Window > Preferences` aller sur `Java > Installed JREs` et 
   ajouter les machines virtuelles java qui manquent.  
   Si besoin configurer la partie `Java > Installed JREs > Execution Environments`.

### Installation avec la *grosse archive*

1. Sur https://www.eclipse.org/downloads/packages/ récupérer le *bundle* 
   `Eclipse IDE for Enterprise Java and Web Developers` correspondant à votre OS.
2. Extraire l'archive
3. Ouvrir le dossier *eclipse* et  lancer l'exécutable (`eclipse.exe`).  
   Remarque : 
   pour installer la version *2024-06* vous aurez besoin d'une machine virtuelle *Java-21*.
4. Configurer les machines virtuelles installées.  
   Dans `Window > Preferences` aller sur `Java > Intalled JREs` et 
   ajouter les machines virtuelles java qui manquent.  
   Si besoin configurer la partie `Java > Intalled JREs > Execution Environments`.


### Installation des plugins pour la gestion des serveurs JEE 

Il est possible de gérer les instances des serveurs d'applications 
directement depuis l'*IDE Eclipse* (*démarrage* / *Arret* / *Déploiements*). 
Cela simplifie bien les choses, entre autres parce que l'on peut avoir 
accès aux logs des serveurs dans l'IDE. 

À cet effet les vendeurs de serveurs d'applications JEE distribuent souvent 
également des plugins spécifiques pour leurs serveurs.

#### Installation du plugin *Wildfly*

Dans l'*IDE Eclipse*, ouvrir le *Marketplace* : `Help > Eclipse Marketplace...`  
Rechercher *Wildfly* et installer *JBoss Tools*.

Remarque : *JBoss Tools* comprend beaucoup de choses, du coup il est un peu gros à télécharger.  
Il est possible de n'installer qu'un sous ensemble des outils JBoss. 

- Pour la gestion des serveurs d'applications 
  (Wildfly / JBoss AS / JBoss EAP) : installer **JBoss AS, WildFly & EAP server Tools**.
- Pour CDI : installer **Contexts and Dependency Injection Tools**.
- etc.

#### Installation du plugin *Payara*

Dans l'*IDE Eclipse*, ouvrir le *Marketplace* : `Help > Eclipse Marketplace...`  
Rechercher *Payara* et installer *Payara Tools*.

### Configuration des serveurs JEE pour Eclipse

### Configuration serveur Wildlfy

Configuration du runtime :   
- Dans les *Préférences d'Eclipse* aller dans `Server > Runtime Environments`
- Cliquer sur le bouton *Add* 
- Choisir `JBoss Community > Wilfly 27+` et cliquer sur bouton *Next*
- Donner un nom à votre runtime, renseigner l'emplacement où vous avez extrait votre serveur d'application 
  et indiquez une JDK compatible (JDK 11 ou 17)
- Changer le fichier de configuration par défaut `standalone.xml` (instance correspondant au *profil Web JEE*) 
  pour ``standalone-full.xml`` (activation plateforme JEE complète)

Configuration des instances : 
- Dans l'onglet *server* (fenêtre en bas à droite - perspective Java EE) - click droit puis *New Server*
- choisir un type de serveur (*JBoss Community > Wilfly 27+*) et le nom de votre serveur puis
- dans l'écran suivant choisir un des runtimes configurés. 

### Configuration serveur *Payara*

Configuration du runtime : 

- Dans les *Préférences d'Eclipse* aller dans `Server > Runtime Environments`
- Cliquer sur le bouton *Add* 
- Choisir `Payara > Payara` et cliquer sur bouton *Next*
- Donner un nom à votre runtime, renseigner l'emplacement où vous avez extrait votre serveur d'application 
  (emplacement du dossier *payara6*) et indiquez une JDK compatible (jdk 11 ou 17)

Configuration des instances : 
- Dans l'onglet *server* (fenêtre en bas à droite - perspective Java EE) - click droit puis *New Server*
- Choisir un type de serveur (*Payara > Payara*) et un des runtimes configurés
- Dans l'écran suivant donner un nom à votre serveur et
  activer la boite à cocher *Enable the Hot Deploy mode* 
  pour ne pas à avoir à redémarrer le serveur à chaque modification. 

### Installation simplifiée avec l'installateur Eclipse :

La configuration suivante permet d'installer l'IDE Eclipse version 2024-06 avec les plugins de gestion des serveurs Wildfy (plugin de gestion des serveurs JBoss de JBoss Tools) et Payara (plugin Payara Tools).

Installation : 
- faites un *drag and drop* du lien ci-contre (<a id="link-drag-and-drop-2024-06" class="button" href="https://gitlab.com/info706_jee/installation/-/raw/master/eclipse_configs/Info706Eclipse202406ConfigurationConfiguration.setup" tooltip="Drag and drop the following link on the installer's title area:
https://gitlab.com/info706_jee/installation/-/raw/master/eclipse_configs/Info706Eclipse202406ConfigurationConfiguration.setup">Drag Link - Eclipse 2024-06</a>) 
vers le haut de l'installeur Eclipse ou
- copiez le lien *ci-dessus* contenant l'URL de la configuration eclipse à installer.  
  Puis dans l'installateur Eclipse cliquez sur l'option `Apply Configuration` (interface simplifiée) ou 
  sur l'icone `Apply the configuration from the clipboard` juste à côté de la zone de recherche (interface avancée).


![copie d'écran interface simplifiée](images/screenshot_basic.png)  
*copie d'écran interface simplifiée*


![copie d'écran interface avancée](images/screenshot_advanced.png)  
*copie d'écran interface avancée*


## Documentation

Jakarta EE 10 (fondation Eclipse) :

- Doc : https://jakarta.ee/resources/#documentation
- API (javadoc) : https://jakarta.ee/specifications/platform/10/apidocs/
- Spécifications : https://jakarta.ee/specifications
- Serveurs compatibles : 
    - https://jakarta.ee/compatibility/certification/10/

Wildfly 33 :

- https://docs.wildfly.org/33/

Payara :

- https://docs.payara.fish/community/docs/Overview.html